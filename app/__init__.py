from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask_login import LoginManager

ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg', 'jpeg'])

app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

login_gerenciador = LoginManager()
login_gerenciador.init_app(app)
login_gerenciador.login_view = 'login'

'''
@login_gerenciador.user_loader
def load_user(user_id):
    return None
'''
from .models import tables
from .controllers import default


