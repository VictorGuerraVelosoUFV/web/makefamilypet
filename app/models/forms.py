from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, IntegerField, validators, DateField, TextAreaField
from wtforms.validators import DataRequired, Email
from wtforms.fields.html5 import EmailField, DateField
from flask_security.forms import Length, PasswordField, Required
from datetime import date
from flask_wtf.file import FileField


password_required = Required(message='PASSWORD_NOT_PROVIDED')
password_length = Length(min=8, max=16, message='PASSWORD_INVALID_LENGTH')

class LoginForm(FlaskForm):
    useremail = EmailField('endereco-email', [DataRequired(), Email()])
    password = PasswordField('senha', validators = [password_required,  password_length])


class AdoteForm(FlaskForm):
    nome_responsavel = StringField("name", validators = [DataRequired()])
    rg =  IntegerField("rg", validators=[DataRequired()])
    telefone = IntegerField("tel", validators=[DataRequired()])
    email_contato = EmailField('email', [DataRequired(), Email()])
    nome_pet = StringField("pet-name", validators=[DataRequired()])


class ReportForm(FlaskForm):
    nome = StringField("name", validators = [DataRequired()])
    telefone = IntegerField("tel", validators=[DataRequired()])
    email_contato = EmailField('email', [DataRequired(), Email()])
    data = DateField('date', default=date.today(), format='%d/%m/%Y', validators=[DataRequired(message="Você precisa informar a data em questão")])
    nome_pet = StringField("pet-name", validators=[DataRequired()])
    condicao = StringField("pet-condition", validators=[DataRequired()])
    file = FileField("pet-pic",validators=[DataRequired()])

class ContaForm(FlaskForm):
    nome = StringField("nome", validators=[DataRequired()])
    useremail = EmailField('email', [DataRequired(), Email()])
    password = PasswordField('password', validators = [password_required,  password_length])
    papel = StringField("papel", validators=[DataRequired()])
    cpf_cnpj = IntegerField("cpf", validators=[DataRequired()])


class AnimalForm(FlaskForm):
    nome = StringField("nome", validators = [DataRequired()])
    data = DateField('date', default=date.today(), format='%d/%m/%Y', validators=[DataRequired(message="Você precisa informar a data em questão")])
    peso = IntegerField("peso", validators=[DataRequired()])
    tamanho = IntegerField("tamanho", validators=[DataRequired()])
    tipo_pelo = StringField("tipopelo", validators=[DataRequired()])
    laudo = TextAreaField("laudo")
    file = FileField("foto", validators=[DataRequired()])