from app import db

class Adocao(db.Model):
    __tablename__= "Adocao"
    ID = db.Column(db.Integer, primary_key=True)
    nome_responsavel = db.Column(db.String(100), nullable=False)
    rg = db.Column(db.String(8), nullable=False)
    telefone = db.Column(db.String(11), nullable=False)
    e_mail = db.Column(db.String(64), nullable=False)
    nome_pet = db.Column(db.String(100), nullable=False)
    ID_animal = db.Column(db.Integer, db.ForeignKey('Animal.ID', ondelete='CASCADE'), nullable=False)
    visto = db.Column(db.Boolean, nullable=False)

    def __init__(self, nome_responsavel, rg, telefone, e_mail, nome_pet, ID_animal):
        self.nome_responsavel = nome_responsavel
        self.rg = rg
        self.telefone = telefone
        self.e_mail = e_mail
        self.nome_pet = nome_pet
        self.ID_animal = ID_animal
        self.visto = False


class Relatorio(db.Model):
    __tablename__= "Relatorio"
    ID = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    telefone = db.Column(db.String(11), nullable=False)
    e_mail = db.Column(db.String(64), nullable=False)
    data = db.Column(db.Date(), nullable=False)
    nome_pet = db.Column(db.String(100), nullable=False)
    condicao = db.Column(db.String(200), nullable=False)
    nome_arquivo = db.Column(db.String(100), nullable=False)
    ID_animal = db.Column(db.Integer, db.ForeignKey('Animal.ID', ondelete='CASCADE'), nullable=False)
    visto = db.Column(db.Boolean, nullable=False)

    def __init__(self, nomme, telefone,  e_mail, data, nome_pte, condicao, nomme_arquivo, ID_animal):
        self.nome = nomme
        self.telefone = telefone
        self.e_mail = e_mail
        self.data = data
        self.nome_pet = nome_pte
        self.condicao = condicao
        self.nome_arquivo = nomme_arquivo
        self.ID_animal = ID_animal
        self.visto = False

class Conta(db.Model):
    __tablename__ = "Conta"
    ID = db.Column(db.Integer, primary_key=True)
    e_mail = db.Column(db.String(64), nullable=False, unique = True)
    senha = db.Column(db.String(16), nullable=False)
    nome = db.Column(db.String(100), nullable=False)
    papel = db.Column(db.String(100), nullable=False)
    cpf_cnpj = db.Column(db.String(14), nullable=False, unique=True)
    ID_proprietario = db.Column(db.Integer, db.ForeignKey('Conta.ID'), nullable=True)
    contas = db.relationship("Conta", foreign_keys = ID_proprietario ,cascade="all, delete-orphan")

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.ID)

    def __init__(self, e_mail, senha, nome, papel, cpf_cnpj, ID_proprietario):
        self.e_mail = e_mail
        self.senha = senha
        self.nome = nome
        self.papel = papel
        self.cpf_cnpj = cpf_cnpj
        self.ID_proprietario = ID_proprietario

class Animal(db.Model):
    __tablename__= "Animal"
    ID = db.Column(db.Integer, primary_key=True)
    nome  = db.Column(db.String(100), nullable=False)
    data = db.Column(db.Date(), nullable=False)
    peso = db.Column(db.Float(), nullable=False)
    tamanho = db.Column(db.Float(), nullable=False)
    tipo_pelo = db.Column(db.String(100), nullable=False)
    laudo = db.Column(db.Text, nullable=True)
    nome_arquivo = db.Column(db.String(100), nullable=False)
    adotado = db.Column(db.Boolean, nullable=True)
    adocao = db.relationship(Adocao, backref="parent", passive_deletes=True)
    relatorio = db.relationship(Relatorio, backref="parent", passive_deletes=True)

    def __init__(self, nome, data, peso, tamanho, tipo_pelo, laudo, nome_arquivo):
        self.nome = nome
        self.data = data
        self.peso = peso
        self.tamanho = tamanho
        self.tipo_pelo = tipo_pelo
        self.laudo = laudo
        self.nome_arquivo = nome_arquivo
        self.adotado = False