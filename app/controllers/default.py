from app import app,db, login_gerenciador, ALLOWED_EXTENSIONS
from flask import render_template, flash, redirect, url_for, session, request
from app.models.forms import LoginForm, AdoteForm, ReportForm, ContaForm, AnimalForm
from app.models.tables import Adocao, Relatorio, Conta, Animal
from flask_login import login_user, logout_user, login_required
from werkzeug.utils import secure_filename
from sqlalchemy import func
from datetime import datetime

@login_gerenciador.user_loader
def load_user(e_mail):
    return Conta.query.filter_by(e_mail  =  e_mail ).first()

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/")
@app.route("/home")
def index():
    return render_template('home.html',page_title="Home")

@app.route("/about")
def about():
    return render_template('about.html',page_title="Sobre")

@app.route("/report", methods = ["POST", "GET"])
def report():
    form = ReportForm()
    if request.method == 'POST':
        try:
            animal = Animal.query.filter_by(ID=int(request.form['id'])).first()
        except:
            flash("Animal informado não encontrado!")
            return redirect(url_for('report'))
        if not animal:
            flash("Animal informado não encontrado!")
            return redirect(url_for('report'))
        # <input type="file" name="pet-pic" runat="server" id="input-pet-pic">
        try:
            file = request.files['pet-pic']
        except:
            file = request.files['file']
        #{{form.file(id="input-pet-pic")}}   
        if file.filename == '':
            flash('Nenhum Arquivo Selecionado!')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            try:
                relatorio = Relatorio(request.form['nome'], request.form['telefone'], request.form['email_contato'],
                            request.form['data'], animal.nome, request.form['condicao'], filename, animal.ID)
                db.session.add(relatorio)
                db.session.commit()
                relatorio2 = db.session.query(Relatorio.ID).all()
                file.save('app/static/img/relatorios/' + str(relatorio2[len(relatorio2)-1][0]) + filename, None)
                relatorio.nome_arquivo = str(relatorio2[len(relatorio2)-1][0]) + filename
                db.session.add(relatorio)
                db.session.commit()
                flash("Relatório Recebido com Sucesso!")
            except Exception as excecao:
                print("ocorreu uma exceção ao inserir um relatório de condição do animal")
                print(f"Tipo da exceção: {type(excecao)}")
                print("Descrição da exceção:\n")
                print(excecao)
                flash("Não foi possível modificar os dados do animal! Dados Inválidos ou imagem muito grande!")

        else:
            flash("Formato de arquivo Inválido!")

        return redirect(url_for('report'))
    else:
        lista_adocao = db.session.query(Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho,
                                        Animal.tipo_pelo, Animal.adotado,func.count(Adocao.rg), Animal.nome_arquivo).outerjoin(Adocao,
                                        Animal.ID == Adocao.ID_animal).group_by(Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho, Animal.tipo_pelo, Animal.adotado).all()
        lista_relatorio = db.session.query(Animal.ID, func.count(Relatorio.e_mail)).outerjoin(Relatorio,
                                           Animal.ID == Relatorio.ID_animal).group_by(Animal.ID).all()

        return render_template('report.html',page_title="Reporte", form = form, adocoes = lista_adocao, relatorios = lista_relatorio)

@app.route("/adopt", methods = ["POST", "GET"])
def adopt():
    form = AdoteForm()
    if request.method == 'POST':
        try:
            animal = Animal.query.filter_by(ID=int(request.form['id'])).first()
        except:
            flash("Animal informado não encontrado!")
            return redirect(url_for('adopt'))
        if not animal:
            flash("Animal informado não encontrado!")
            return redirect(url_for('adopt'))
        try:
            adocao = Adocao(request.form['nome_responsavel'], request.form['rg'], request.form['telefone'],
                            request.form['email_contato'], animal.nome, animal.ID)
            db.session.add(adocao)
            db.session.commit()
            flash("Pedido Recebido com Sucesso!")
            return redirect(url_for('adopt'))
        except Exception as excecao:
            print("ocorreu uma exceção ao inserir um pedido de adoção")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)
            flash("Não foi possível processar o pedido! Dados Inválidos ou já existentes!")
            return redirect(url_for('adopt'))
    else:
        lista_adocao = db.session.query(Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho,Animal.tipo_pelo,Animal.adotado,
                                        func.count(Adocao.rg), Animal.nome_arquivo).outerjoin(Adocao,Animal.ID == Adocao.ID_animal).group_by(
                                        Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho,Animal.tipo_pelo,Animal.adotado).all()

        lista_relatorio = db.session.query(Animal.ID, func.count(Relatorio.e_mail)).outerjoin(Relatorio,
                                            Animal.ID == Relatorio.ID_animal).group_by(Animal.ID).all()
        return render_template('adopt.html',page_title="Adoção", form = form, adocoes = lista_adocao, relatorios = lista_relatorio)

@app.route("/login", methods = ["POST", "GET"])
def login():
    form = LoginForm()
    if request.method == 'POST':
        conta = Conta.query.filter_by( e_mail = request.form['useremail']).first()
        if conta and conta.senha == request.form['password']:
            login_user(conta)
            session['logged_in'] = True
            session['logged_id'] = conta.ID
            return redirect(url_for('feed'))
        else:
            flash("Login e/ou senha inválido(s)!")
            return redirect(url_for('login'))

    else:
        return render_template('login.html',page_title="Login", form = form)

@app.route("/logout", methods = ["GET"])
def logout():
    logout_user()
    session['logged_in'] = False
    return redirect(url_for('index'))

@app.errorhandler(404)
def not_found(e):
    return render_template('404.html',page_title="404 Not Found")

@app.route("/admin/feed", methods = ["POST", "GET"])
def feed():
    if not session.get('logged_in'):
        return redirect(url_for('login'))
    else:
        if request.method == 'GET':
            lista_adocao = db.session.query(Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho,Animal.tipo_pelo, Animal.adotado,
                                            Adocao.nome_responsavel, Adocao.rg, Adocao.telefone, Adocao.e_mail, Adocao.visto, Adocao.ID, Animal.nome_arquivo).outerjoin(Adocao, Animal.ID == Adocao.ID_animal).group_by(
                                            Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho, Animal.tipo_pelo, Animal.adotado,
                                            Adocao.nome_responsavel, Adocao.rg, Adocao.telefone, Adocao.e_mail, Adocao.visto, Adocao.ID, Animal.nome_arquivo).all()
            lista_relatorio = db.session.query(Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho,Animal.tipo_pelo, Animal.adotado,
                                               Relatorio.nome, Relatorio.telefone, Relatorio.e_mail, Relatorio.data,Relatorio.condicao, Relatorio.visto, Relatorio.ID, Relatorio.nome_arquivo).outerjoin(Relatorio,Animal.ID == Relatorio.ID_animal).group_by(
                                               Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho, Animal.tipo_pelo, Animal.adotado,
                                               Relatorio.nome, Relatorio.telefone, Relatorio.e_mail, Relatorio.data, Relatorio.condicao, Relatorio.visto, Relatorio.ID, Relatorio.nome_arquivo).all()
            return render_template('feed.html',page_title="Feed", adocoes = lista_adocao, relatorios = lista_relatorio)

        else:
            if request.json[1] == 'adocao':
                adocao = Adocao.query.filter_by(ID=int(request.json[0])).first()
                try:
                    adocao.visto = True
                    db.session.add(adocao)
                    db.session.commit()
                    return ('1')
                except Exception as excecao:
                    print("ocorreu uma exceção ao modificar o valor da adoção para visto")
                    print(f"Tipo da exceção: {type(excecao)}")
                    print("Descrição da exceção:\n")
                    print(excecao)
                    return('0')

            else:
                relatorio = Relatorio.query.filter_by(ID=int(request.json[0])).first()
                try:
                    relatorio.visto = True
                    db.session.add(relatorio)
                    db.session.commit()
                    return ('1')
                except Exception as excecao:
                    print("ocorreu uma exceção ao modificar o valor do relatório para visto")
                    print(f"Tipo da exceção: {type(excecao)}")
                    print("Descrição da exceção:\n")
                    print(excecao)
                    return ('0')

@app.route("/admin/accounts/delete", methods = ["POST", "GET"])
def delete():
    if request.method == 'GET':
        return redirect(url_for('accounts'))
    else:
        conta = Conta.query.filter_by(ID=session['logged_id']).first()
        db.session.delete(conta)
        db.session.commit()
        return redirect(url_for('logout'))


@app.route("/admin/accounts/edit", methods = ["POST", "GET"])
def edit():
    if request.method == 'GET':
        return redirect(url_for('accounts'))
    else:
        if len(request.form['cpf_cnpj']) != 11 and len(request.form['cpf_cnpj']) != 14:
            flash("CPF/CNPJ em formato inválido!")
            return redirect(url_for('accounts'))
        conta = Conta.query.filter_by(ID=session['logged_id']).first()
        try:
            conta.e_mail = request.form['useremail']
            conta.senha = request.form['password']
            conta.nome = request.form['nome']
            conta.papel = request.form['papel']
            conta.cpf_cnpj = request.form['cpf_cnpj']
            db.session.add(conta)
            db.session.commit()
            flash("Conta Modificada com Sucesso!")
            return redirect(url_for('accounts'))
        except Exception as excecao:
            print("ocorreu uma exceção ao modificar a conta")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)
            flash("Não foi possível modificar a conta! Dados Inválidos ou já existentes!")
            return redirect(url_for('accounts'))

@app.route("/admin/accounts", methods = ["POST", "GET"])
def accounts():
    if not session.get('logged_in'):
        return redirect(url_for('login'))
    else:
        form = ContaForm()
        if request.method == 'POST':
            if len(request.form['cpf_cnpj']) != 11 and len(request.form['cpf_cnpj']) != 14:
                flash("CPF/CNPJ em formato inválido!")
                return redirect(url_for('accounts'))
            try:
                conta = Conta(request.form['useremail'], request.form['password'], request.form['nome'],
                                request.form['papel'], request.form['cpf_cnpj'], session['logged_id'])
                db.session.add(conta)
                db.session.commit()
                flash("Conta cadastrada com Sucesso!")
                return redirect(url_for('accounts'))
            except Exception as excecao:
                print("ocorreu uma exceção ao inserir uma conta")
                print(f"Tipo da exceção: {type(excecao)}")
                print("Descrição da exceção:\n")
                print(excecao)
                flash("Não foi possível cadastrar a conta! Dados Inválidos ou já existentes!")
                return redirect(url_for('accounts'))
        else:
            conta = Conta.query.filter_by(ID=session['logged_id']).first()
            return render_template('accounts.html',page_title="Gerência de Contas", form = form, conta = conta)



@app.route("/admin/animals/delete", methods = ["POST", "GET"])
def delete_animal():
    if request.method == 'GET':
        return redirect(url_for('animals'))
    else:
        animal = Animal.query.filter_by(ID=int(request.json)).first()
        db.session.delete(animal)
        db.session.commit()
        return redirect(url_for('animals'))

@app.route("/admin/animals/edit", methods = ["POST", "GET"])
def edit_animal():
    if request.method == 'GET':
        return redirect(url_for('animals'))
    else:
        animal = Animal.query.filter_by(ID=request.form['id']).first()
        try:
            file = request.files['foto']
        except:
            file = request.files['file']
            # {{form.file(id="input-pet-pic")}}
        if file.filename == '':
            flash('Nenhum Arquivo Selecionado!')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
        try:
            animal.nome = request.form['nome']
            animal.data = request.form['data']
            animal.peso = request.form['peso']
            animal.tamanho = request.form['tamanho']
            animal.tipo_pelo = request.form['tipo_pelo']
            animal.laudo = request.form['laudo']
            animal.nome_arquivo = str(animal.ID) + filename
            db.session.add(animal)
            file.save('app/static/img/animais/' + str(animal.ID) + filename, None)
            db.session.commit()
            flash("Dados do Animal Modificados com Sucesso!")
            return redirect(url_for('animals'))
        except Exception as excecao:
            print("ocorreu uma exceção ao modificar o animal")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)
            flash("Não foi possível modificar os dados do animal! Dados Inválidos ou imagem em formato inválido!")
            return redirect(url_for('animals'))


@app.route("/admin/animals", methods = ["POST", "GET"])
def animals():
    if not session.get('logged_in'):
        return redirect(url_for('login'))
    else:
        form = AnimalForm()
        if request.method == 'POST':
            try:
                file = request.files['foto']
            except:
                file = request.files['file']
                # {{form.file(id="input-pet-pic")}}
            if file.filename == '':
                flash('Nenhum Arquivo Selecionado!')
                return redirect(request.url)

            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
            try:
                animal = Animal(request.form['nome'], request.form['data'], request.form['peso'],
                                request.form['tamanho'], request.form['tipo_pelo'], request.form['laudo'], filename)
                db.session.add(animal)
                db.session.commit()
                animal2 = db.session.query(Animal.ID).all()
                file.save('app/static/img/animais/' + str(animal2[len(animal2)-1][0]) + filename, None)
                animal.nome_arquivo = str(animal2[len(animal2) - 1][0]) + filename
                db.session.add(animal)
                db.session.commit()
                flash("Animal cadastrado com Sucesso!")
                return redirect(url_for('animals'))
            except Exception as excecao:
                print("ocorreu uma exceção ao inserir um animal")
                print(f"Tipo da exceção: {type(excecao)}")
                print("Descrição da exceção:\n")
                print(excecao)
                flash("Não foi possível cadastrar o animal! Dados Inválidos ou já existentes!")
                return redirect(url_for('animals'))

        else:
            lista_adocao = db.session.query(Animal.ID, Animal.nome, Animal.data, Animal.peso, Animal.tamanho, Animal.tipo_pelo,
                                            Animal.adotado, Animal.laudo ,func.count(Adocao.rg), Animal.nome_arquivo).outerjoin(
                                            Adocao, Animal.ID == Adocao.ID_animal).group_by( Animal.ID, Animal.nome, Animal.data,
                                            Animal.peso, Animal.tamanho, Animal.tipo_pelo,Animal.adotado, Animal.laudo).all()

            lista_relatorio = db.session.query(Animal.ID, func.count(Relatorio.e_mail)).outerjoin(Relatorio,
                                               Animal.ID == Relatorio.ID_animal).group_by(Animal.ID).all()

            return render_template('animals.html', page_title="Gerência de PETS", form = form, adocoes = lista_adocao,  relatorios = lista_relatorio)



@app.route("/admin/aadopt/delete", methods = ["POST", "GET"])
def delete_adocao():
    if request.method == 'GET':
        return redirect(url_for('aadopt'))
    else:
        adocao = Adocao.query.filter_by(ID=int(request.json)).first()
        db.session.delete(adocao)
        db.session.commit()
        return redirect(url_for('aadopt'))


@app.route("/admin/aadopt/update", methods = ["POST", "GET"])
def update_adocao():
    if request.method == 'GET':
        return redirect(url_for('aadopt'))
    else:
        adocao = Adocao.query.filter_by(ID=int(request.json)).first()
        animal = Animal.query.filter_by(ID=int(adocao.ID_animal)).first()
        if not animal.adotado:
            animal.adotado = True
        else:
            animal.adotado = False
        db.session.add(animal)
        db.session.commit()
        return redirect(url_for('aadopt'))


@app.route("/admin/adopt", methods = ["POST", "GET"])
def aadopt():
    if not session.get('logged_in'):
        return redirect(url_for('login'))
    else:
        form = AdoteForm()
        if request.method == 'POST':
            adocao = Adocao.query.filter_by(ID=request.form['id']).first()
            try:
                adocao.nome_responsavel = request.form['nome_responsavel']
                adocao.rg = request.form['rg']
                adocao.telefone = request.form['telefone']
                adocao.e_mail = request.form['email_contato']
                adocao.nome_pet = request.form['nome_pet']
                db.session.add(adocao)
                db.session.commit()
                flash("Dados da Adoção Modificados com Sucesso!")
                return redirect(url_for('aadopt'))
            except Exception as excecao:
                print("ocorreu uma exceção ao modificar o animal")
                print(f"Tipo da exceção: {type(excecao)}")
                print("Descrição da exceção:\n")
                print(excecao)
                flash("Não foi possível modificar os dados da adoção! Dados Inválidos!")
                return redirect(url_for('aadopt'))
        else:
            lista_adocao = db.session.query(Adocao.ID, Adocao.nome_responsavel, Adocao.rg, Adocao.telefone, Adocao.e_mail, Adocao.nome_pet, Adocao.ID_animal,
                                            Animal.adotado, Animal.nome_arquivo).outerjoin(Animal, Animal.ID == Adocao.ID_animal).all()
            return render_template('aadopt.html',page_title="Gerência de adoção", form = form, adocoes = lista_adocao)