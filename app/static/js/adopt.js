/*import closeAllModals, {openModal, closeModal} from "./modal.js";

var getAnimal = document.getElementById("getAnimal");
var getAnimalButton = document.getElementById("getAnimalButton");
getAnimalButton.addEventListener('click',()=>openModal(getAnimal),false);
function select(nome){
    closeModal(getAnimal);
    document.getElementById('input-pet-name').value = nome;
}
window.select = select;


var perfilPublico = document.getElementById("perfilPublico");
var perfilPublicoButton = document.getElementsByClassName("perfilPublicoButton");
Array.from(perfilPublicoButton).forEach((el)=>el.addEventListener("click",(e)=>openModal(perfilPublico),false));

var backButton = document.getElementsByClassName("card-back")[0];
backButton.addEventListener("click",()=>{
    closeModal(perfilPublico);
    document.getElementsByTagName("body")[0].style.overflow = "hidden";
},false);


closeAllModals(()=>{closeModal(getAnimal);closeModal(perfilPublico)});*/

$(document).ready(function() {
    $("#animais").hide();
    $(".dados-animal").hide();
    $('#getAnimalButton').on('click', function(){
        $("#animais").show();
        $('html, body').animate({
          scrollTop: 0
          }, 1000, 'linear');
    });
    $('.fas').on('click', function(){
        $("#animais").hide();
        $(".dados-animal").hide();
	  });
});

function exibirDados(idanimal) {
    $("#"+idanimal).show();
}

function ocultarDados(idanimal) {
  $("#"+idanimal).hide();
}

function realizarAdocao(idanimal, item) {
  var nome = document.getElementsByTagName("h1")[item].textContent;
  document.getElementById("input-pet-name").value = nome;
  document.getElementById("oculto").value = idanimal;
  $("#animais").hide();
}