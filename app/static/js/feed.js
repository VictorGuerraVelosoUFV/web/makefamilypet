$(document).ready(function() {
    $("#animais").hide();
    $("#animais2").hide();
    $(".dados-animal").hide();
    $(".dados-animal2").hide();
    $("#relatorios").hide();
    $("#relatorios2").hide();
    $(".dados-relatorio").hide();
    $(".dados-relatorio2").hide();
    $('#exibirAdocoes').on('click', function(){
        $("#animais2").hide(); 
        $("#animais").show();
        $(".dados-animal2").hide();
        $("#relatorios").hide();
        $("#relatorios2").hide();
        $(".dados-relatorio").hide();
        $(".dados-relatorio2").hide();   
    });
    $('#exibirNaoVisto').on('click', function(){
        $("#animais").hide(); 
        $("#animais2").show();
        $(".dados-animal").hide();
        $("#relatorios").hide();
        $("#relatorios2").hide();
        $(".dados-relatorio").hide();
        $(".dados-relatorio2").hide();   
    });
    $('.fas').on('click', function(){
            $("#animais").hide();
            $(".dados-animal").hide();
            $("#animais2").hide();
            $(".dados-animal2").hide();
            $("#relatorios").hide();
            $("#relatorios2").hide();
            $(".dados-relatorio").hide();
            $(".dados-relatorio2").hide(); 
      });
      
      $('#exibirRelatorio').on('click', function(){
        $("#animais2").hide(); 
        $("#animais").hide();
        $(".dados-animal2").hide();
        $("#relatorios").show();
        $("#relatorios2").hide();
        $(".dados-relatorio").hide();
        $(".dados-relatorio2").hide();   
    });
    $('#exibirNaoVistoRelatorio').on('click', function(){
        $("#animais").hide(); 
        $("#animais2").hide();
        $(".dados-animal").hide();
        $("#relatorios").hide();
        $("#relatorios2").show();
        $(".dados-relatorio").hide();
        $(".dados-relatorio2").hide();   
    });
});

function exibirDados(idanimal) {
    $("#"+idanimal).show();
}

function ocultarDados(idanimal) {
  $("#"+idanimal).hide();
}

function marcarVisto(idAdocao, categoria) {
    $.ajax({
        url: '/admin/feed',
        type: 'POST',
        data: JSON.stringify([idAdocao, categoria], null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(response) {
            if(response == "1"){
                alert("Alteração feita com sucesso!");
            }
            else{
                alert("Uma falha impediu que a  alteração fosse feita");
            }
            console.log(response);
            window.location.reload();
        },
        error: function(error) {
            console.log(error);
        }
    });
  }