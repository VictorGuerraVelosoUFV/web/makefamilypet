/*import closeAllModals, {openModal, closeModal} from "./modal.js";

var accountManagement = document.getElementById("accountManagement");
closeAllModals(()=>closeModal(accountManagement));
var accountManagementButton = document.getElementsByClassName("accountManagementButton");
var addAccountButton = document.getElementById("addAccountButton");
Array.from(accountManagementButton).forEach((e)=>e.addEventListener('click',()=>openModal(accountManagement),false));
addAccountButton.addEventListener('click',()=>openModal(accountManagement),false);*/

$(document).ready(function() {
	$("#accountManagement").hide();
	$("#accountedit").hide();
	$("#deletar").hide();
    $('#addAccountButton').on('click', function(){
		$("#adicao").hide();
		$("#accountManagement").show();
	});
	$('#cancelar').on('click', function(){
		$("#adicao").show();
		$("#accountManagement").hide();
	});
	$('#fechar').on('click', function(){
		$("#adicao").show();
		$("#accountManagement").hide();
	});
	$('#editar').on('click', function(){
		$("#accountedit").show();
		$("#adicao").hide();
	});
	$('#cancelar2').on('click', function(){
		$("#adicao").show();
		$("#accountedit").hide();
	});
	$('#fechar2').on('click', function(){
		$("#adicao").show();
		$("#accountedit").hide();
	});
	$('#excluir').on('click', function(){
		if (confirm("tem certeza que deseja excluir sua conta?")){
			$.ajax({
				url: '/admin/accounts/delete',
				data: '',
				type: 'POST',
				success: function() {
					window.location.reload();
					console.log("Exclusão com sucesso!");
				},
				error: function(error) {
					console.log(error);
				}
			});
	  	}
	});
});