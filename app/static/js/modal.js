var registerModals = function(closeModals){
    // When the user clicks on (x), close the modal
    Array.from(document.getElementsByClassName("card-action")).forEach(
        (e)=>e.addEventListener("click",closeModals,false));
    // When the user clicks anywhere outside of the modal, close it
    Array.from(document.getElementsByClassName("modal")).forEach(
        (e)=>e.addEventListener("click",closeModals,false));
    // When user click inside of the modal, it should not close it
    Array.from(document.getElementsByClassName("modal-content")).forEach(
        (e)=>e.addEventListener("click",(event)=>{event.stopPropagation();},false)); 
}
var openModal = function(modal){
    modal.style.display = "block";
    document.getElementsByTagName("body")[0].style.overflow = "hidden";
}
var closeModal = function(modal){
    modal.style.display = "none";
    document.getElementsByTagName("body")[0].style.overflow = "auto";
}
export {openModal, closeModal};
export default registerModals;