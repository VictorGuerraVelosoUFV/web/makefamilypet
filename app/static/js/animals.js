/*import closeAllModals, {openModal, closeModal} from "./modal.js";
// Get modals
var perfilPrivado = document.getElementById("perfilPrivado");
var perfilPublico = document.getElementById("perfilPublico");
var addAnimal = document.getElementById("addAnimal");
var closeModals = function(){
    closeModal(perfilPrivado);
    closeModal(perfilPublico);
    closeModal(addAnimal);
}

var stretchTextArea = function(elem){
    elem.style.overflow = "hidden";
    elem.style.height = "";
    elem.style.height = elem.scrollHeight + "px";
}

// When the user clicks the buttons, open the modals
var laudomedico = document.getElementById("laudomedico");
laudomedico.addEventListener("input",()=>stretchTextArea(laudomedico));

var perfilPrivadoButton = document.getElementsByClassName("perfilPrivadoButton");
var perfilPublicoButton = document.getElementsByClassName("perfilPublicoButton");
var addAnimalButton = document.getElementById("addAnimalButton");
var perfilPrivadoButtonClick = function(event){
    event.preventDefault(); 
    event.stopPropagation(); 
    openModal(perfilPrivado);
    stretchTextArea(laudomedico);
}
var perfilPublicoButtonClick = function(event){
    event.preventDefault(); 
    event.stopPropagation();
    openModal(perfilPublico);
}
addAnimalButton.addEventListener("click", (event)=>{
    event.preventDefault(); 
    event.stopPropagation();
    openModal(addAnimal);
},false);

Array.from(perfilPrivadoButton).forEach(
    (e)=>e.addEventListener("click",perfilPrivadoButtonClick,false));
Array.from(perfilPublicoButton).forEach(
    (e)=>e.addEventListener("click",perfilPublicoButtonClick,false));

closeAllModals(closeModals);*/

$(document).ready(function() {
	$("#addAnimal").hide();
    $('#addAnimalButton').on('click', function(){
		$("#adicao").hide();
		$("#addAnimal").show();
	});
	$('#cancelar').on('click', function(){
		$("#adicao").show();
		$("#addAnimal").hide();
	});
	$('#fechar').on('click', function(){
		$("#adicao").show();
		$("#addAnimal").hide();
    });
    
    $("#animais").hide();
    $(".dados-animal").hide();
    $(".editAnimal").hide();
    $('#listarAnimais').on('click', function(){
        $("#animais").show();
        $('html, body').animate({
          scrollTop: 0
          }, 1000, 'linear');
    });
    $('.fas').on('click', function(){
        $("#animais").hide();
        $(".dados-animal").hide();
	  });
});

function exibirDados(idanimal) {
    $("#"+idanimal).show();
    $("#"+idanimal + 's').hide();
}

function ocultarDados(idanimal) {
  $("#"+idanimal).hide();
}

function editarDados(idanimal) {
    if ($("#"+idanimal + 's').is(':visible')){
        $("#"+idanimal + 's').hide();
    }
    else {
        $("#"+idanimal + 's').show();
        $("#"+idanimal).hide();
    }
}

function excluirDados(idanimal) {
    if (confirm("tem certeza que deseja excluir o cadastro?")){
        $.ajax({
            url: '/admin/animals/delete',
            type: 'POST',
            data: JSON.stringify(idanimal, null, '\t'),
            contentType: 'application/json;charset=UTF-8',
            success: function() {
                window.location.reload();
                console.log("Exclusão com sucesso!");
            },
            error: function(error) {
                console.log(error);
            }
        });
      }
}
  