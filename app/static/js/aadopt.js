/*import closeAllModals, {openModal, closeModal} from "./modal.js";

var addAdoption = document.getElementById("addAdoption");
closeAllModals(()=>closeModal(addAdoption));
var editAdoptionButton = document.getElementsByClassName("editAdoptionButton");
var addAdoptionButton = document.getElementById("addAdoptionButton");
Array.from(editAdoptionButton).forEach((e)=>e.addEventListener('click',()=>openModal(addAdoption),false));
addAdoptionButton.addEventListener('click',()=>openModal(addAdoption),false)*/

$(document).ready(function() {
    $(".dados-adocao").hide();
    $(".editAdocao").hide();
});

function exibirDados(idanimal) {
    if ($("#"+idanimal).is(':visible')){
        $("#"+idanimal).hide();
    }
    else {
        $("#"+idanimal).show();
        $("#"+idanimal + 's').hide();
    }
}

function editarDados(idanimal) {
    if ($("#"+idanimal + 's').is(':visible')){
        $("#"+idanimal + 's').hide();
    }
    else {
        $("#"+idanimal + 's').show();
        $("#"+idanimal).hide();
    }
}

function excluirDados(idanimal) {
    if (confirm("tem certeza que deseja excluir o registro?")){
        $.ajax({
            url: '/admin/aadopt/delete',
            type: 'POST',
            data: JSON.stringify(idanimal, null, '\t'),
            contentType: 'application/json;charset=UTF-8',
            success: function() {
                window.location.reload();
                console.log("Exclusão com sucesso!");
            },
            error: function(error) {
                console.log(error);
            }
        });
      }
}

function confirmarDados(idanimal) {
    if (confirm("tem certeza que deseja alterar o status do pedido?")){
        $.ajax({
            url: '/admin/aadopt/update',
            type: 'POST',
            data: JSON.stringify(idanimal, null, '\t'),
            contentType: 'application/json;charset=UTF-8',
            success: function() {
                window.location.reload();
                console.log("Atualização com sucesso!");
            },
            error: function(error) {
                console.log(error);
            }
        });
      }
}